#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy as sp
from scipy import stats
import collections

class Tree():
    _DURATION = 12*24
    _PATIENT0_HIST_OPTS = ['equil', 'none', 'legacy']
    _R_NORM = stats.skewnorm.pdf(np.arange(0,12,1./24), 1., 4., 2.5)/24

    def __init__(self, patient0_hist='none', test_patient0=False, G=None,
                 R0=2, delay=6, loss=0.1, iso_fac=0.2, test_fac=0.1):
        G = self._init_patient0_hist(G, patient0_hist, test_patient0, R0=R0)
        self._init_tree(G)
        self._init_model_props(R0, delay, loss, iso_fac, test_fac)
        self._init_analysis()

        # print('init totals = ', self.totals)
        # print('init node num = ', len(self._G.nodes))

    def _init_patient0_hist(self, G, patient0_hist, test_patient0, **kwargs):
        self._patient0_hist = patient0_hist
        self._test_patient0 = test_patient0
        if (G is not None) and (patient0_hist is not None):
            # print('Ignore patient0_hist option, using initial graph tree G instead.')
            return G

        if patient0_hist in ['none', 'legacy']:
            G = None

        elif patient0_hist=='equil':
            tree0 = Tree.get_equil_infection_hist(**kwargs)

            test_time = Tree.get_patient0_test_time()

            params_no_testing = kwargs.copy()
            params_no_testing['delay'] = 0
            params_no_testing['loss'] = 0
            params_no_testing['test_fac'] = 0

            tree = Tree(G=tree0.G, **params_no_testing)
            tree.run_sim(tspan_days=test_time/24, verbose=False)

            ancestor_root, recovered_nodes = Tree.get_ancestor_root(tree)

            pruned_G, pruned_active_nodes = (
                Tree.prune_ancestor_root(tree, ancestor_root, recovered_nodes))

            # Reset clock to 0 at test-time for patient0
            for node_id in pruned_G.nodes:
                pruned_G.nodes[node_id]['t_infect'] -= test_time

            G = pruned_G

            # ancestor_root, recovered_nodes  = Tree.get_ancestor_root(tree)
            # G_prune, pruned_active_nodes = Tree.prune_ancestor_root(tree, ancestor_root, recovered_nodes)
            # G_prune
            # G = G_prune
            self._test_patient0 = True

        else:
            # assert patient0_hist in self._PATIENT0_HIST_OPTS, (
            assert False, (
                'That is not a valid choice for patient0_hist. Choose from : '
                + str(self._PATIENT0_HIST_OPTS ))

        return G

    def _init_analysis(self):
        hist_infected = self._G.number_of_nodes()
        totals = {'hist_infected' : hist_infected, 'active':0,
                  'infected' : 0, 'spontaneous' : 0, 'tested' : 0,
                  'recovered': 0, 'infected_by_recovered': 0}

        totals_history = {
            'active':[], 'infected' : [], 'spontaneous' : [], 'tested' : []}
        self._totals = totals
        self._totals_history = totals_history


    def _init_tree(self, G):
        self._G = G
        self._clock = 0
        self._cur_node = 0
        self._news = None

        if self._G is None:
            self._G = nx.DiGraph()
            self._add_new_infection(np.nan)

        self._cur_node = np.max(list(self._G.nodes()))+1
        # Nnode = self._G.number_of_nodes()
        # self._cur_node = 1
        # print('nodes: ', [inode for inode in self._G.nodes])
        # print('curr node: ', self._cur_node)

        assert type(self._G) is nx.DiGraph, 'G is not a valid DiGraph.'

    def _init_model_props(self, R0, delay, loss, iso_fac, test_fac):
        R_norm = self._R_NORM

        R_inf = R0*R_norm
        R_iso = iso_fac*R_inf
        R_test = test_fac*R_norm

        self._R0 = R0
        self._delay = delay
        self._loss = loss

        self._iso_fac = iso_fac
        self._test_fac = test_fac

        # self._R_norm = R_norm
        self._R_inf = R_inf
        self._R_iso = R_iso
        self._R_test = R_test

    @property
    def DURATION(self):
        return self._DURATION

    @property
    def G(self):
        return self._G

    @property
    def R0(self):
        return self._R0

    @property
    def delay(self):
        return self._delay

    @property
    def loss(self):
        return self._loss

    @property
    def R_inf(self):
        return self._R_inf

    @property
    def R_iso(self):
        return self._R_iso

    @property
    def R_test(self):
        return self._R_test

    @property
    def totals(self):
        return self._totals

    ################################################
    def get_equil_infection_hist(**kwargs):
        # must be true right now
        relabel=True

        while True:
            tree = Tree(test_fac=0, **kwargs)
            tree.run_sim(tspan_days=tree.DURATION/24+1, verbose=False)
            tree.totals
            if tree.totals['active']==0:
                continue

            tree = Tree._expand_to_patient0(tree)
            if tree is None:
                continue
            else:
                break

        if relabel:
            G = tree.G
            node_ids0= np.array(G.nodes)
            # print(node_ids0)
            # print(node_ids0)
            node_remap = dict(zip(node_ids0, node_ids0-node_ids0.size+1))
            # print(node_remap)

            # NOTE: do NOT use copy=False... there is a bug that reorders nodes
            H = nx.relabel_nodes(G, node_remap, copy=True)

            patient0 = H.nodes[0]
            # may have off-by one error
            t_patient0 = patient0['t_infect']
            # print('t_patient0 = ', t_patient0)

            for node_id in H.nodes:
                H.nodes[node_id]['t_infect'] -= t_patient0

            # print('t_patient0 update = ', H.nodes[0])

            tree._G = H

        return tree

    def _expand_to_patient0(tree):
        infected0 = tree.totals['infected']
        infected = infected0

        while infected==infected0:
            tree.run_sim(tspan_days=1/24, verbose=False)
            infected = tree.totals['infected']
            active = tree.totals['active']
            if active==0:
                return None

        return tree

    def get_ancestor_root(tree):
        G = tree.G
        Nnode = G.number_of_nodes()
        recovered = np.tile(False, Nnode)
        node_ids = np.zeros(Nnode, dtype=int)

        for ind, (inode, irecov) in enumerate(G.nodes(data='recovered')):
            # ind = inode+Nnode-1
            # print('inode = ', inode)
            # print('ind = ', ind)
            node_ids[ind] = inode
            recovered[ind] = irecov

        recovered_nodes = node_ids[recovered]

        branch_roots = []
        for inode in recovered_nodes:
            ineigh = list(nx.neighbors(G, inode))
            unrecovered_neigh = [ind for ind in ineigh if ind not in recovered_nodes]
            branch_roots.extend(unrecovered_neigh)

        branch_roots = np.array(branch_roots)

        # mask = np.array([0 in nx.descendants(G, i) for i in branch_roots])

        mask = np.tile(False, len(branch_roots))
        for ind, iroot in enumerate(branch_roots):
            idescendants = nx.descendants(G,iroot)
            if (iroot==0) or (0 in idescendants):
                mask[ind] = True


        ancestor_root = branch_roots[mask][0]

        return ancestor_root, recovered_nodes

    def get_ancestor_root_legacy(tree):
        G = tree.G

        Nnode = G.number_of_nodes()
        recovered = np.tile(False, Nnode)
        node_ids = np.zeros(Nnode, dtype=int)
        for inode, irecov in G.nodes(data='recovered'):
            ind = inode+Nnode-1
            node_ids[ind] = inode
            recovered[ind] = irecov

        #print(recovered)
        #print(node_ids)
        recovered_nodes = node_ids[recovered]
        # print(recovered_nodes)

        branch_roots = []
        for inode in recovered_nodes:
            ineigh = list(nx.neighbors(G, inode))
            unrecovered_neigh = [ind for ind in ineigh if ind not in recovered_nodes]
            branch_roots.extend(unrecovered_neigh)

        branch_roots = np.array(branch_roots)
        # print(branch_roots)

        mask = np.array([0 in nx.descendants(G, i) for i in branch_roots])
        ancestor_root = branch_roots[mask][0]

        return ancestor_root, recovered_nodes

    def prune_ancestor_root(tree, ancestor_root, recovered_nodes):
        G = tree.G
        nodes = list(nx.descendants(G, ancestor_root))+[ancestor_root]
        nodes = sorted(nodes)

        pruned_nodes = [i for i in G.nodes if i not in nodes]
        pruned_active_nodes = [i for i in pruned_nodes
                               if i not in recovered_nodes]
        if len(pruned_active_nodes)==0:
            pruned_active_nodes = None


        pruned_G = G.copy()
        pruned_G.remove_nodes_from([n for n in pruned_G
                                    if n not in set(nodes)])
        # pruned_frozen_G = nx.subgraph(G, nodes)

        # unfreeze graph by making copy
        # pruned_G = nx.DiGraph(pruned_frozen_G)
        # pruned_G = nx.DiGraph(G)
        return pruned_G, pruned_active_nodes

    def get_patient0_test_time():
        R_norm = Tree._R_NORM
        R_test_norm = R_norm/R_norm.sum()
        R_test_cum = np.cumsum(R_test_norm)
        test_time_patient0 = np.where(np.random.rand()< R_test_cum)[0][0]
        return test_time_patient0

    #####################################


    def run_sim(self, tspan_days=7, verbose=True):
        test_patient0 = self._test_patient0

        tspan = tspan_days*24
        test_queue = collections.defaultdict(list)

        if verbose:
            print("Starting simulation for %.2f days\n" % tspan_days)
        else:
            self._news = None

        if test_patient0:
            # test_time_patient0 = Tree.get_patient0_test_time()
            test_time_patient0 = 0
            todays_tests = self._schedule_tests(
                test_queue, 0, test_time=test_time_patient0)

            if verbose:
                print('Schedule patient0 test for ', test_time_patient0)

        for t_dont_use in np.arange(tspan):
            if verbose:
                self._news = ""
            infected = self._get_infected()

            # if verbose:
            #     print('current node = ', self._cur_node)

            # if verbose:
            #     print('infected = ', infected)

            # todays_tests = []
            todays_tests = test_queue[self._clock]
            for case in infected:
                # spread new infections, but no intant testing
                # if verbose:
                #     print('nodes: ', [inode for inode in self._G.nodes])
                #     print('case = ', case)

                self._spread_infection(case)
                # todays_tests = test_queue[self._clock]
                if self._attempt_test(case):
                    todays_tests = self._schedule_tests(test_queue, case)

            test_queue = self._run_scheduled_tests(
                todays_tests, test_queue)

            #    print("day %d" % (t/24))
            if (verbose and self._news != ""):
                print("%d:%s" % (self._clock, self._news))
            self._clock += 1

            self._totals_history['infected'].append(self._cur_node)
            self._totals_history['active'].append(len(self._get_infected()))
            self._totals_history['spontaneous'].append(
                self._totals['spontaneous'])
            self._totals_history['tested'].append(self._totals['tested'])


        # self._totals['infected'] = self._G.number_of_nodes() # maybe unnecessary
        self._totals['infected'] = self._G.number_of_nodes() - self._totals['hist_infected'] # maybe unnecessary
        self._totals['active'] = len(self._get_infected())


    def _get_infection_prob(self, t_elapsed, iso):
        if (t_elapsed >= self._DURATION) or (t_elapsed<0):
            print("get_infection_prob t_elapsed out of bounds %d\n" % t_elapsed)
            return 0
        if iso:
            return self._R_iso[t_elapsed]
        else:
            return self._R_inf[t_elapsed]
        return prob

    # checks for new infection, or recovery
    def _attempt_new_infection(self, case):
        icase = self._G.nodes[case]
        t_infect = icase['t_infect']
        t_elapsed = self._clock-t_infect
        if t_elapsed < self._DURATION:
            prob_infect = self._get_infection_prob(t_elapsed, icase['isolated'])
            new_infection = np.random.rand()<=prob_infect
            return new_infection
        else:
            icase['recovered'] = True
            if self._news is not None:
                self._news += ' R%d' % case
            self._totals['recovered'] += 1
            self._totals['infected_by_recovered'] += len(self._G.succ[case])
            # the following line is just for debugging.
            # print("Recovered %d %d %d" % (case, self._totals['recovered'], self._totals['infected_by_recovered']))
            return False

    def _add_new_infection(self, source):
        self._G.add_node(self._cur_node, t_infect=self._clock, recovered=False, isolated=False)
        if self._cur_node>0:
            self._G.add_edge(source, self._cur_node)
        if self._news is not None:
            self._news += ' I%d->%d' % (source, self._cur_node)
        self._cur_node += 1


    def _get_infected(self):
        node_ids = np.array([node for node in self._G.nodes])
        mask_recovered = np.array(
            [inode[1] for inode in self._G.nodes(data='recovered')])
        infected_mask = np.where(~mask_recovered)[0]
        infected = node_ids[infected_mask]
        return infected

    def _get_contacts(self, case):
        successors = [i for i in self._G.successors(case)]
        predecessors = [i for i in self._G.predecessors(case)]
        contact_ids = successors+predecessors
        return contact_ids

    def _attempt_test(self, case):
        node = self._G.nodes[case]
        if node['recovered'] or node['isolated']:
            return False
        t_elapsed = self._clock - node['t_infect']
        if t_elapsed<0 or t_elapsed>=self._DURATION:
            print("attempt_test t_elapsed out or bounds")
            return False
        if np.random.rand()<=self._R_test[t_elapsed]:
            return True
        else:
            return False

    # plan testing of contacts.  Don't bother filtering by tested/recovered/etc since that will happen later.
    def _trace_contacts(self, case):
        contact_ids = self._get_contacts(case)
        num_contacts = len(contact_ids)
        success_mask = (np.random.rand(num_contacts) > self._loss)
        # later can replace by variable delay
        plans = [(contact_ids[x], self._clock + self._delay) for x in np.arange(num_contacts) if success_mask[x]]

        if self._news is not None:
            self._news += "".join(map(lambda x : " CT%d->%d (%d)" % (case, x[0], x[1]), plans))
        return plans

    # labels a node as tested, returns list of scheduled tests
    def _perform_test(self, case):
        icase = self._G.nodes[case]
        if icase['isolated'] or icase['recovered']:
            return False, []

        icase['isolated'] = True
        if self._news is not None:
            self._news += ' T%d' % case

        plans = self._trace_contacts(case)
        return True, plans

    def _spread_infection(self, case):
        infect_contact = self._attempt_new_infection(case)
        if infect_contact:
            self._add_new_infection(case)

    def _schedule_tests(self, test_queue, case, test_time=None):
        if test_time is None:
            test_time = self._clock

        # plan_test = self._attempt_test(case)
        todays_tests = test_queue[test_time]
        # if plan_test:

        todays_tests += [case]
        self._totals['spontaneous'] += 1

        # test_queue[test_time] = todays_tests

        return todays_tests

    def _run_scheduled_tests(self, todays_tests, test_queue):
        if self._news is not None and len(todays_tests)>0:
            self._news += ' ...run_Ts:'

        for tested in todays_tests:
            test_occured, new_contacts = self._perform_test(tested)
            self._totals['tested'] += test_occured
            for plan_id,plan_time in new_contacts:
                test_queue[plan_time].append(plan_id)

        return test_queue
