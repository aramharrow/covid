# Contact-tracing model for COVID-19

Technical details are found in [ct_model.pdf](../tables/ct_model.pdf).

Run `python get_stats.py` to perform the simulation and output
statistics.
The other relevant files are `events.py` which contains the model and
`model.py` which is an old version that is no longer used by
default. The two files should produce equivalent results but
`events.py` is faster.
Some input parameters are in `config.yml` and some can be set by the
command line.

## Installation
You will need to install these python packages:
matplotlib, networkx, numpy, scipy, tabulate

You will also need [latex](https://www.latex-project.org/get/), or
should set `latex_output` to false.
