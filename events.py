# this is where priorityqueue stuff is going.  eventually will replace model.py

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy as sp
from scipy import stats
import collections
from queue import PriorityQueue

def sample_curve(params):
    time = -1
    while (time < 0 or time > params['duration']):
        time = stats.skewnorm.rvs(1., 4., 2.5)
    return time

STATUS_ACTIVE = 0
STATUS_ISOLATED = 1
STATUS_RECOVERED = 2

# contains the state of a simulation
class Simulation():
    def __init__(self, general, params):
        # self._init_model_props
        self.params = params
        self.general = general
        if general['patient0_hist'] == 'equil':
            print("Warning: equilibrum initialization not implemented.")
        if general['test_patient0']:
            print("Warning: patient-0 testing not implemented.")
        self.events = PriorityQueue()

        # G = self._init_patient0_hist(G, patient0_hist, test_patient0, R0=R0)
        # self._init_tree(G)
        self._G = nx.DiGraph()
        self._cur_node = 0
        self._clock = 0
        self.status = []
        # self._init_analysis()
        hist_infected = self._G.number_of_nodes()
        self.totals = {'hist_infected' : hist_infected, 'active':0, 'infected' : 0,
        'spontaneous' : 0, 'tested' : 0, 'recovered': 0, 'infected_by_recovered': 0}
        self.add_new_infection(np.nan)

    # only called by add_new_infection
    def _forecast(self, case_id):
        n_infect_always = np.random.poisson(lam = self.params['R0'] * self.params['iso_fac'])
        n_infect_untested = np.random.poisson(lam = self.params['R0'] * (1-self.params['iso_fac']))
        if self.general['verbose']:
            print("Performing forecast for %d with %d, %d infections" % (case_id, n_infect_always, n_infect_untested))

        for n in range(n_infect_always):
            when = self._clock + sample_curve(self.params)
            self.events.put((when, ('infect_always', case_id)))
            if self.general['verbose']:
                print("Infect even if tested from %d at time %0.2f" % (case_id, when))

        when_tested = None
        if (np.random.rand() < self.params['test_fac']):
            when_tested = self._clock + sample_curve(self.params)
            self.events.put((when_tested, ('test', case_id, True)))
            if self.general['verbose']:
                print("Test %d (spontaneously) at time %0.2f" % (case_id, when_tested))

        for n in range(n_infect_untested):
            when = self._clock + sample_curve(self.params)
            if (when_tested != None) and (when > when_tested):
                continue
            self.events.put((when, ('infect_untested', case_id)))
            if self.general['verbose']:
                print("Infect if untested from %d at time %0.2f" % (case_id, when))

        when = self._clock + self.params['duration']
        self.events.put((when, ('recover', case_id)))
        if self.general['verbose']:
            print("Recovery of %d scheduled for %0.2f" % (case_id, when))

    def add_new_infection(self, source):
        self._G.add_node(self._cur_node) #, t_infect=self._clock, recovered=False, isolated=False)
        self.status.append(STATUS_ACTIVE)
        if self._cur_node>0:
            self._G.add_edge(source, self._cur_node)
        self._forecast(self._cur_node)  # this is the only function that calls _forecast
        self._cur_node += 1
        self.totals['active'] += 1
        self.totals['infected'] += 1  # this should always equal _cur_node

    def perform_test(self, case, spontaneous):
        if self.status[case] != STATUS_ACTIVE:
            return
        self.status[case] = STATUS_ISOLATED
        # once merge is complete, I'd prefer to replace 'tested' with 'traced', which would be tested-spontaneous in the old variables
        self.totals['tested'] += 1
        if spontaneous:
            self.totals['spontaneous'] += 1
        #_trace_contacts(self, case):
        for contact in nx.all_neighbors(self._G, case):
            if self.status[contact] == STATUS_ACTIVE:
                if np.random.rand() > self.params['loss']:
                    # later change delay time to hours
                    when_tested = self._clock + self.params['delay']/24
                    self.events.put((when_tested, ('test', contact, False)))
                    if self.general['verbose']:
                        print("Day %0.2f: schedule test for %d (from %d) at time %0.2f" % (self._clock, contact, case, when_tested))

    def recover(self, case):
        assert self.status[case] != STATUS_RECOVERED
        self.status[case] = STATUS_RECOVERED
        if self.general['verbose']:
            print("Day %0.2f, case %d recovered." % (self._clock, case))
        self.totals['recovered'] += 1
        self.totals['infected_by_recovered'] += self._G.out_degree(case)
        self.totals['active'] -= 1

    def run_sim(self):
        end_date = self._clock + self.general['num_days']
        while (not self.events.empty()):
            (when, (what, who, *how)) = self.events.get()
            if self.general['verbose']:
                print((when, (what, who, *how)))
            assert(when >= self._clock)
            if (when > end_date):
                self.events.put((when, (what, who, *how)))
                break
            self._clock = when

            if (what == 'infect_always') or ((what == 'infect_untested') and self.status[who]==STATUS_ACTIVE):
                self.add_new_infection(who)
            elif (what == 'test'):
                self.perform_test(who, how[0])
            elif (what == 'recover'):
                self.recover(who)
