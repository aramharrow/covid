import model as m
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy as sp
from scipy import stats
import collections
import copy

scenarios = [ (m.Tree(**{'R0':2, 'delay':1000, 'loss':1, 'iso_fac':0.2, 'test_fac':0.2, 'G':None}), "No contact tracing", "none"),\
(m.Tree(**{'R0':2, 'delay':48, 'loss':0.1, 'iso_fac':0.2, 'test_fac':0.2, 'G':None}), "Slow contact tracing", "slow"),\
(m.Tree(**{'R0':2, 'delay':6, 'loss':0.1, 'iso_fac':0.2, 'test_fac':0.2, 'G':None}), "Fast contact tracing", "fast"), \
(m.Tree(**{'R0':2, 'delay':1, 'loss':0, 'iso_fac':0.2, 'test_fac':0.2}), "Super-fast contact tracing", "super") ]



# params = {'R0':2, 'delay':1, 'loss':0, 'iso_fac':0.2, 'test_fac':0.2}
# tree = m.Tree(**params)
# tree.run_sim(tspan_days=20, verbose=True)
# tree.totals

tree = scenarios[3][0]
tree.run_sim(tspan_days=20, verbose=True)
print(tree.totals['infected'])
