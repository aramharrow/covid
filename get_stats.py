import sys
import model as m
import events as ev
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy as sp
from scipy import stats
from tabulate import tabulate
import subprocess
import time
import csv
import yaml
import getopt
import pathlib
#import collections
#import copy
#from multiprocessing import Process, Queue

def underline(str):
    return str + "\n" + '-'*len(str)

config = yaml.safe_load(open("config.yml"))
general = config['general']
defaults = config['default']
scenarios_mod = config['scenarios']

try:
    opts, args = getopt.getopt(sys.argv[1:],"n:d:R:l:i:t:veq",["ifile=","ofile="])
except getopt.error as err:
    print("""%s
    Valid command-line options are:
    [-n num_samples] Change number of samples (default %s)
    [-d num_days] Change number of days (default %s)
    [-R R0] Change R0 (default %.1f)
    [-l loss] Change fraction of lost contacts (default %.2f)
    [-i iso] Change isolate effectiveness (default %.2f)
    [-t test] Change probability of spontaneous testing (default %.2f)
    [-v] Verbose
    [-e] Use equilibrum patient history (default patient history is 'none')
    [-q] Use priority queue model
    """ % (err, general['num_samples'], general['num_days'], defaults['R0'], defaults['loss'], defaults['iso_fac'], defaults['test_fac'] ))
    sys.exit(2)

for opt, arg in opts:
    if opt == '-n':
        general['num_samples'] = arg
    elif opt == '-d':
        general['num_days'] = arg
    elif opt == '-v':
        general['verbose'] = True
    elif opt == '-e':
        general['patient0_hist'] = 'equil'
    elif opt == '-R':
        defaults['R0'] = float(arg)
    elif opt == '-l':
        defaults['loss'] = float(arg)
    elif opt == '-i':
        defaults['iso_fac'] = float(arg)
    elif opt == '-t':
        defaults['test_fac'] = float(arg)
    elif opt == '-q':
        general['queue'] = True

scenarios = [{**defaults, **x} for x in scenarios_mod]

if general['latex_output'] or general['csv_output'] or general['full_output']:
    output_dir = 'output'
    pathlib.Path(output_dir).mkdir(exist_ok=True)

general['num_samples'] = int(general['num_samples'])
general['num_days'] = int(general['num_days'])

general_summary = "Simulation results for %d samples of %d days with patient0-history=%s." % (general['num_samples'], general['num_days'], general['patient0_hist'])
if general['queue']:
    general_summary += "\nUsing priority queue."
if general['text_output']:
    print(general_summary)

latex_output=  r"""\documentclass[12pt]{article}
\usepackage{booktabs}
\begin{document}
%s
\begin{center}
Model parameters

""" % general_summary

tab = [(scen['desc'], scen['R0'], scen['delay'], scen['loss'], scen['iso_fac'], scen['test_fac']) for scen in scenarios]
headers = ["models",  "R0", "delay", "loss", "isolate", "test"]
if general['text_output']:
    print(tabulate(tab, headers))
latex_output += tabulate(tab, headers, tablefmt="latex_booktabs")
latex_output += "\n\\end{center}\n\n"

results = {}

for scen in scenarios:
    if general['verbose']:
        print(scen['desc'])
    results[scen['name']] = {'hist_infected' : [], 'infected' : [], 'active' : [],
    'spontaneous' : [], 'tested' : [], 'recovered': [], 'infected_by_recovered': []}
    for iter in range(general['num_samples']):
        # sim = m.Tree(**scen)
        if general['queue']:
            sim = ev.Simulation(general, scen)
            sim.run_sim()
        else:
            sim = m.Tree(patient0_hist=general['patient0_hist'], test_patient0=general['test_patient0'],
                R0=scen['R0'], delay=scen['delay'], loss=scen['loss'], iso_fac=scen['iso_fac'], test_fac=scen['test_fac'])
            sim.run_sim(general['num_days'], general['verbose'])

        for (desc, num) in sim.totals.items():
            results[scen['name']][desc].append(num)
        if general['verbose']:
            print(sim.totals['infected'], end = ' ')
            if (iter % 50 == 49):
                print()
    if general['verbose']:
        print()

if general['verbose']:
    print()

reporting_categories = {'infected' : 'Total infections throughout simulation', \
'active' : 'Active infections at end of simulation',\
'spontaneous' : 'Number of spontaneous tests', \
'tested' : 'Total number of tests'}

for (name, desc) in reporting_categories.items():
    if general['text_output']:
        print("\n" + underline(desc))
    latex_output += "\\begin{center}\n" + desc + "\n\n"
    tab = []
    for (scen, outcomes) in results.items():
        data = np.array(outcomes[name])
        tab.append([scen, np.quantile(data, 0.25), np.median(data), np.quantile(data, 0.75), np.mean(np.log(1+data)), np.std(np.log(1+data))])
        if general['full_output']:
            f = open(output_dir+"/out-%s-%s.txt" % (name,scen), "a")
            f.write("".join(("%d\n"%x) for x in data))
            f.close()
    headers = ["model", "25%", "50%", "75%", "mean(log)", "stdev(log)"]
    floatfmt = ("f", ".0f", ".0f", ".0f", ".2f", ".2f")
    if general['text_output']:
        print(tabulate(tab, headers, floatfmt=floatfmt))
    latex_output += tabulate(tab, headers, floatfmt=floatfmt, tablefmt="latex_booktabs")
    latex_output += "\\end{center}"

if general['csv_output']:
    filename='R0_'+str(defaults['R0'])+'_loss_'+str(defaults['loss'])+'_isofac_'+str(defaults['iso_fac'])+'.csv'
    print("\nWriting data to " + filename + "\n")
    with open('output/'+filename, 'w') as fid:
        dat_writer=csv.writer(fid)
        dat_writer.writerow(headers)
        dat_writer.writerows(tab)

# estimate Re
tab = []
headers = ["model", "R_eff", "total recovered patients"]
floatfmt = ("f", ".2f", ".0f")
latex_output += "\\begin{center}\n Estimate of $R_e$ \n\n"
for (scen, outcomes) in results.items():
    num_recovered = sum(outcomes['recovered'])
    Re = sum(outcomes['infected_by_recovered']) / num_recovered
    tab.append([scen, Re, num_recovered])

if general['text_output']:
    print(tabulate(tab, headers, floatfmt=floatfmt))
latex_output += tabulate(tab, headers, floatfmt=floatfmt, tablefmt="latex_booktabs")
latex_output += "\\end{center}"
latex_output += "\\end{document}\n"

if general['latex_output']:
    f = open(output_dir+'/results.tex', 'w');
    f.write(latex_output)
    f.close()
    curtime = time.strftime("%Y%m%d-%H%M%S",time.localtime())
    subprocess.call("pdflatex -jobname=output/results-%s output/results.tex > output/log.txt" % curtime, shell=True)
    subprocess.call("rm output/results-%s.aux" % curtime, shell=True)
    subprocess.call("rm output/results-%s.log" % curtime, shell=True)
