\documentclass{article}
\usepackage{hyperref}
\begin{document}
\title{Technical details of contact-tracing model}
\author{Gretchen Aleks, Aram Harrow, Shefali Oza, and Aaron Wolf}
\maketitle

This is a simple model that is meant to simulate the effects of contact-tracing programs of varying speed and quality.

The model begins with a single infected patient. It assumes that each infection lasts for 12 days. Each step of the simulation represents one hour.  The adjustable parameters are:
\begin{itemize}
\item $R_0$: expected number of infections from a non-isolated patient (default 2.0)

\item {\em isolate}: Isolated patients have their R0 multiplied by this variable (default 0.1)

\item {\em test}: Probability that a patient will spontaneously be tested (i.e. without having been reached through contact tracing).  (default 0.2)

\item {\em delay}: Delay in hours between testing one patient and testing their contacts. (default: 1, 24, 48 or infinity, meaning no contact tracing is performed)

\item {\em loss}: Expected fraction of contacts that are {\em not} traced.  (default: 0.1)
\end{itemize}

The strength of the infection (both in terms of contagiousness and probability of leading to testing) is modeled by the curve in Fig 1 of [Quantifying SARS-CoV-2 transmission suggests epidemic control with digital contact tracing;  \href{https://science.sciencemag.org/content/early/2020/03/30/science.abb6936/}{ Science 31 Mar 2020}].  This curve defines a distribution $f(t)$ where $t$ is the number of hours, and $\sum_t f(t)=1$.

Each time step consists of the following.
\begin{itemize}
\item Each patient causes a new infection with probability $R_0 \cdot  f(t)$ for non-isolated patients or $R_0 \cdot \mathrm{isolate} * f(t)$ for isolated patients.  Here $t$ is the number of hours that this patient has been infected.

\item Each patient is marked as recovered if they have been infected for 12 days.

\item Each untested patient is tested with a probability $\mathrm{test} * f(t)$.  Each tested patient isolates until they recover.  Also, each contact of a tested patient is scheduled to be tested after $\mathtt{delay}$ hours with probability $1-\mathtt{loss}$.
\end{itemize}

\paragraph{Limitations}
\begin{itemize}
\item The parameters may not be accurate.  The paper describing $f(t)$ is based on only 40 events.  The parameters isolate, loss, test may be plausible but they are not based on any data.

\item This is a simple model that does not incorporate many of the features of more complicated models, such as stratifying the population into age groups with different mixing rates, or accounting for hospitalization.  It assumes an infinite population so that infection rates do not decrease as the number of recovered cases increases.

\item The value of $R_0=2$ will depend on the policies being enacted, and the level of compliance with them.  These may change over time and may vary with different subpopulations.

\item This does not model capacity limits for testing or contact tracing.  For this reason, we do not consider contacts that are not positive, since their only effect is on using up the time of testers and contact tracers.

\item The output of this model has high variance, as is expected for this type of process.  We have not done a statistical analysis of whether our number of repetitions can catch relevant rare events.

\item Inaccurate tests are not considered.

\item After 12 days, we assume that patients are not only recovered but also test negative and do not participate in contact tracing.  These patients may still test positive, and even if not, are likely to remember being symptomatic, and thus could usefully participate in contact tracing.
\end{itemize}
\end{document}