#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy as sp
from scipy import stats
import collections

# we could bundle these into a 'params' data structure
#def init_globals():
#    global DURATION, R0, R_INF, R_ISO, R_TEST, DELAY, LOSS, VERBOSE
DURATION = 12*24
INIT_PATIENT_PROGRESS = 0  # 3*24
R0 = 2.0
R_INF = R0*stats.skewnorm.pdf(np.arange(0,12,1./24), 1., 4., 2.5)/24
R_ISO = R_INF * 0.2
R_TEST = R_INF * 0.05
DELAY = 36
LOSS = 0
PATIENT_ZERO = 0 # id of first patient

def get_infection_prob(t_elapsed, iso):
    if (t_elapsed >= DURATION) or (t_elapsed<0):
        print("We should never reach this line %d\n" % t_elapsed)
        return 0
    if iso:
        return R_ISO[t_elapsed]
    else:
        return R_INF[t_elapsed]
    return prob

def init_sim():
    G = nx.DiGraph()
    add_new_infection(PATIENT_ZERO, -INIT_PATIENT_PROGRESS, np.nan, G, t_test=np.nan)
    return G

# checks for new infection, or recovery
def attempt_new_infection(t, case, G, news):
    icase = G.nodes[case]
    t_infect = icase['t_infect']
    t_elapsed = t-t_infect
    if t_elapsed < DURATION:
        prob_infect = get_infection_prob(t_elapsed, icase['isolated'])
        new_infection = np.random.rand()<=prob_infect
        return new_infection, news
    else:
        icase['recovered'] = True
        if news is not None:
            news += ' R%d' % case
        return False, news

def add_new_infection(node_id, t, source, G, news=None, t_test=np.nan):
    G.add_node(node_id, t_infect=t, t_test=t_test, recovered=False, isolated=False)
    if node_id>PATIENT_ZERO:
        G.add_edge(source, node_id, t_contact=t)

    if news is not None:
        news += ' I%d->%d' % (source, node_id)
        return news

def get_infected(G):
    mask_recovered = np.array(
        [inode[1] for inode in G.nodes(data='recovered')])
    infected = np.where(~mask_recovered)[0]+PATIENT_ZERO
    return infected

def get_contacts(G, case):
    successors = [i for i in G.successors(case)]
    predecessors = [i for i in G.predecessors(case)]
    contact_ids = successors+predecessors
    # contact_data = [G.nodes[i] for i in contact_ids]
    return contact_ids

def attempt_test(t, case, G, news):
    node = G.nodes[case]
    if node['recovered'] or node['isolated'] or ~np.isnan(node['t_test']):
        return False, news
    t_elapsed = t - node['t_infect']
    if t_elapsed<0 or t_elapsed>=DURATION:
        print("should never see this")
        return False, news
    if np.random.rand()<=R_TEST[t_elapsed]:
        return True, news
    else:
        return False, news

# plan testing of contacts.  Don't bother filtering by tested/recovered/etc since that will happen later.
def trace_contacts(t, case, G, news):
    contact_ids = get_contacts(G, case)
    num_contacts = len(contact_ids)
    success_mask = (np.random.rand(num_contacts) > LOSS)
    # pythonic enough?  or should it be a dictionary instead of an array, e.g. {'contact': 5, 'time': 33}
    plans = [(contact_ids[x], t + DELAY) for x in np.arange(num_contacts) if success_mask[x]]

    if news is not None:
        news += "".join(map(lambda x : " CT%d->%d (%d)" % (case, x[0], x[1]), plans))
    return plans, news

# labels a node as tested, returns list of scheduled tests
def perform_test(t, case, G, news):
    icase = G.nodes[case]
    if ~np.isnan(icase['t_test']) or icase['recovered']:
        return False, [], news

    icase['t_test'] = t
    icase['isolated'] = True  # maybe redundant if only tested people are isolated
    if news is not None:
        news += ' T%d' % case

    plans, news = trace_contacts(t, case, G, news)
    return True, plans, news

def spread_infection(node_id, t, case, G, news):
    infect_contact, news = attempt_new_infection(t, case, G, news)
    if infect_contact:
        node_id += 1
        news = add_new_infection(node_id, t, case, G, news)
    return node_id, news

def schedule_tests(test_queue, t, case, G, news,  totals):
    plan_test, news = attempt_test(t, case, G, news)
    todays_tests = test_queue[t]
    if plan_test:
        todays_tests += [case]
        totals['spontaneous'] += 1

    # test_queue[t] = todays_tests
    # return test_queue, news
    return todays_tests, totals, news

def run_scheduled_tests(todays_tests, test_queue, t, G, news, totals):
    # todays_tests = test_queue[t]

    if news is not None and len(todays_tests)>0:
        news += ' ...run_Ts:'

    for tested in todays_tests:
        test_occured, new_contacts, news = perform_test(t, tested, G, news)
        totals['tested'] += test_occured
        # test_queue += new_contacts
        for plan_id,plan_time in new_contacts:
            test_queue[plan_time].append(plan_id)

    return test_queue, totals, news

def run_sim(tspan_days=7, verbose=True):
    G = init_sim()
    tspan = tspan_days*24
    test_queue = collections.defaultdict(list)
    node_id = PATIENT_ZERO
    totals = {'infected' : 0, 'spontaneous' : 0, 'tested' : 0}

    if verbose:
        print("Starting simulation for %d days\n" % tspan_days)
    else:
        news = None

    for t in np.arange(tspan):
        if verbose:
            news = ""
        infected = get_infected(G)

        for case in infected:
            node_id, news = spread_infection(node_id, t, case, G, news)

            todays_tests, totals, news = schedule_tests(
                test_queue, t, case, G, news,  totals)

        test_queue, totals, news = run_scheduled_tests(
            todays_tests, test_queue, t, G, news, totals)


        #    print("day %d" % (t/24))
        if (verbose and news != ""):
            print("%d:%s" % (t, news))

    totals['infected'] = node_id # maybe unnecessary

    return G, totals
