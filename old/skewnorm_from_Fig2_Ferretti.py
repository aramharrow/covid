import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

R0=2
deltat=1./24
x=np.arange(0,12,deltat)
a=1. #skewness
loc=4. #peak location for skew=0
scale=2.5 #stdev
pdf=R0*stats.skewnorm.pdf(x, a, loc, scale)

plt.plot(x, pdf)
plt.grid()

print(x[np.argmax(pdf)]) #check day of maximum -- seems a bit later than Ferretti paper
print(np.sum(pdf)*deltat) #check that it integrates to R0 value
